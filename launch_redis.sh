#!/bin/bash
#==============================================================
#  launch_redis.sh
#
#    auteur: Anthony Bouteloup
#    crée le: 14.11.19
#
#    Ce script sert à lancer automatiquement
#    l'installation et le lancement d'un serveur redis
#==============================================================



#--------- mise à jour de la machine -------------
sudo yum update
#--------- installation de Redis -----------------
sudo amazon-linux-extras install redis4.0
#--Changement de la configuration de Redis avec l'ip privé:--
ip_prive=$(ip addr show | grep -o "inet 172.[0-9]*\.[0-9]*\.[0-9]*" | grep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*")
sudo sed -i "/^bind/c\bind $ip_prive" /etc/redis.conf
#--------- lancement de redis -------------------
sudo systemctl enable redis
sudo systemctl start redis


