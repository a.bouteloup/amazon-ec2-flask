#!/bin/bash
#==============================================================
#  launch_flask.sh
#
#    auteur: Anthony Bouteloup
#    crée le: 14.11.19
#
#    Ce script sert à lancer automatiquement
#    l'installation et le lancement d'un serveur flask
#==============================================================



#--------- mise à jour de la machine -------------
sudo yum update
#--------- installation de Redis -----------------
sudo amazon-linux-extras install redis4.0
#--------- installation de python ----------------
sudo yum install python37
curl -O https://bootstrap.pypa.io/get-pip.py
python3 get-pip.py --user
pip install flask --user
pip install redis --user

#--------- lancement du programme flask ----------
python3 app.py
